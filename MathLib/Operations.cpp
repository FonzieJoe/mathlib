#include <math.h>
#include <vector>
namespace operations {

	float derivative(float(*function)(float), float value) { //https://en.wikipedia.org/wiki/Difference_quotient
		float inc = 0.01;
		float retval = function(value + inc) - function(value);
		retval /= inc;
		return retval;
	}

	float integral(float(*function)(float), float init, float end, int numdiv) { //area under a curve 
		//https://upload.wikimedia.org/wikipedia/commons/thumb/f/f2/Integral_as_region_under_curve.svg/2000px-Integral_as_region_under_curve.svg.png
		float delta = (init - end) / (float)numdiv; //size of interval
		std::vector<float> Yvalues;
		std::vector<float> middleValues;
		float retval = 0;
		Yvalues.resize(numdiv);
		for (int i = 0; i < Yvalues.size(); i++) Yvalues[i] = function(i*delta);

		middleValues.resize(floor(numdiv / 2));
		for (int i = 0; i < middleValues.size(); i++) middleValues[i] = (Yvalues[i]+Yvalues[i+1])/2.0f;

		for (int i = 0; i < middleValues.size(); i++) retval += delta*middleValues[i];

		return retval;
	}
}