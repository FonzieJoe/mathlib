#pragma once
#include "GeometryClass.h"
#include "PlaneClass.h"

class TriangleClass :
	public GeometryClass
{
public:
	glm::vec3 vertex1, vertex2, vertex3, normal;
	TriangleClass();
	TriangleClass(const glm::vec3& point0, const glm::vec3& point1, const glm::vec3& point2);
	~TriangleClass();
	void setPosition(const glm::vec3& newPos);
	bool isInside(const glm::vec3& point);
	bool intersecSegment(const glm::vec3& point1, const glm::vec3& point2, glm::vec3& pTall);
	glm::vec3 PlanoPertenecienteTriangulo(); 
	bool InterseccionRectaTriangulo(glm::vec3 Punto);
};

