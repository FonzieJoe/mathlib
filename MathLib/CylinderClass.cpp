#include "CylinderClass.h"



CylinderClass::CylinderClass() : p1(PointClass(0, 0, 0)), p2(PointClass(0, 1, 0)), rad(1), line(LineClass(p1, p2))
{
}

CylinderClass::CylinderClass(PointClass _p1, PointClass _p2, float _rad): p1(_p1), p2(_p2), rad(_rad), line(LineClass(p1,p2))
{
}


CylinderClass::~CylinderClass()
{
}

bool CylinderClass::isInside(const glm::vec3 & point)
{
	if (abs(dist2point(point) - rad) < Tollerance) return (line.alphaPoint(point)>=0 && line.alphaPoint(point)<=1);
}

bool CylinderClass::intersectionLine(LineClass line, glm::vec3 ptall[])
{
	float distance=this->line.distLine2Line(line);
	if ( ( abs(glm::cross(this->line.direction, line.direction).length()) < Tollerance )&&( abs(distance - rad) < Tollerance) )// paralelo
	{
		return true;
	}
	else
	{
		if (distance<=rad+Tollerance) {
			return (abs(line.distLine2Point(line.point) - Tollerance) < rad) != abs((line.distLine2Point(line.point.position+line.direction) - Tollerance) < rad) || abs(line.distLine2Point(line.point) - Tollerance) > rad != abs((line.distLine2Point(line.point.position + line.direction) - Tollerance) > rad);
		}
	}
	return false;
}

bool CylinderClass::intersectionPlane(glm::vec3 ptall[], PlaneClass plane)
{
	return false;
}

float CylinderClass::dist2point(PointClass p)
{
	return line.distLine2Point(p.position);
}

float CylinderClass::dist2line(LineClass l)
{
	float distRecta = line.distLine2Line(l);
	float distPOrigen = l.distLine2Point(p1.position);
	float distPExtremo = l.distLine2Point(p2.position);

	if (distRecta < Tollerance) {
		glm::vec3 temp;
		line.Line2LineIntersection(l, temp);
		float alpha = line.alphaPoint(temp);
		if (!(alpha <= 1 && alpha >= 0)) {
			distRecta = std::numeric_limits<float>::infinity(); //igual a infinito
		}
	}

	if (distRecta < distPOrigen) {
		if (distRecta < distPExtremo) {
			return distRecta;
		}
		else {
			return distPExtremo;
		}
	}
	else {
		if (distPOrigen < distPExtremo) {
			return distPOrigen;
		}
		else {
			return distPExtremo;
		}
	}
}

float CylinderClass::dist2Plane(PlaneClass p)
{
	float distRecta=p.distLine2Plane(line);
	float distPOrigen = p.distPoint2Plane(p1.position);
	float distPExtremo = p.distPoint2Plane(p2.position);

	if (distRecta < Tollerance) {
		glm::vec3 temp;
		p.intersecLinePlane(line, temp);
		float alpha=line.alphaPoint(temp);
		if (!(alpha <= 1 && alpha >= 0)) {
			distRecta = std::numeric_limits<float>::infinity(); //igual a infinito
		}
	}

	if (distRecta < distPOrigen) {
		if (distRecta < distPExtremo) {
			return distRecta;
		}
		else {
			return distPExtremo;
		}
	}
	else{
		if (distPOrigen < distPExtremo) {
			return distPOrigen;
		}
		else {
			return distPExtremo;
		}
	}
}

float CylinderClass::dist2Cylinder(CylinderClass c)
{
	float distRecta = line.distLine2Line(c.line);
	float distPOrigen1 = line.distLine2Point(c.p1.position);
	float distPExtremo1 = line.distLine2Point(c.p1.position);
	float distPOrigen2 = c.line.distLine2Point(p1.position);
	float distPExtremo2 = c.line.distLine2Point(p1.position);

	if (distRecta < Tollerance) {
		glm::vec3 temp;
		c.line.Line2LineIntersection(line, temp);
		float alpha = line.alphaPoint(temp);
		if (!(alpha <= 1 && alpha >= 0)) {
			distRecta = std::numeric_limits<float>::infinity(); //igual a infinito
		}
	}

	if (distPOrigen1 > distPOrigen2) distPOrigen1 = distPOrigen2;
	if (distPExtremo1 > distPExtremo2) distPExtremo1 = distPExtremo2;


	if (distRecta < distPOrigen1) {
		if (distRecta < distPExtremo1) {
			return distRecta;
		}
		else {
			return distPExtremo1;
		}
	}
	else {
		if (distPOrigen1 < distPExtremo1) {
			return distPOrigen1;
		}
		else {
			return distPExtremo1;
		}
	}
}

void CylinderClass::setPosition(const glm::vec3 & newPos)
{
}
