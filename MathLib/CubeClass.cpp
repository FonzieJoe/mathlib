#include "CubeClass.h"
#include <iostream>

void CubeClass::calcMiddlePoint()
{
	middlePoint = glm::vec3(0, 0, 0);
	for (glm::vec3 &dir : direction) middlePoint += dir;
	middlePoint /= 8;
}

CubeClass::CubeClass()
{
}

CubeClass::CubeClass(glm::vec3 dir[8]) :square{ Square(0,1, 2, 3) ,  Square(1,5,6,2), Square(5,6,7,4), Square(4,7,3,0), Square(4,5,1,0), Square(3,2,6,7) },  //abcd cara frontal   //efgh cara trasera (sentido antiorario empezando por la esquina inferior izquierda)
	sides{ LineClass(dir[0], dir[1] - dir[0]),
		   LineClass(dir[1], dir[2] - dir[1]), 
		   LineClass(dir[2], dir[3] - dir[2]),
		   LineClass(dir[3], dir[0] - dir[3]),
		   LineClass(dir[1], dir[5] - dir[1]), 
		   LineClass(dir[5], dir[6] - dir[5]),
		   LineClass(dir[6], dir[2] - dir[6]),
		   LineClass(dir[5], dir[4] - dir[5]), 
		   LineClass(dir[4], dir[7] - dir[4]),
		   LineClass(dir[7], dir[6] - dir[7]),
		   LineClass(dir[0], dir[4] - dir[0]),
		   LineClass(dir[3], dir[7] - dir[3])} //lineas

{
	for (int i = 0; i < 8; i++) //points
		direction[i] = dir[i];
	/*
   7-------6
  /|      /|
 / |     / |
3--|----2  |
|  4----|--5
| /     | /
0-------1
	*/
	/*
	square[0] = Square(0,1, 2, 3);//frontal
	square[1] = Square(1,5,6,2); //L.Der
	square[2] = Square(5,6,7,4);//trasero
	square[3] = Square(4,7,3,0);//L.Izq
	square[4] = Square(4,5,1,0);//inf
	square[5] = Square(3,2,6,7);//supr
	*/

	//planes
	for (int i = 0; i < 6; i++)
		square[i].Face=PlaneClass(direction[(int)square[i].VertexIndex.x], glm::cross(direction[(int)square[i].VertexIndex.y]- direction[(int)square[i].VertexIndex.x], direction[(int)square[i].VertexIndex.z] - direction[(int)square[i].VertexIndex.x]));
		
	calcMiddlePoint();

}

CubeClass::~CubeClass()
{
}

void CubeClass::setPosition(const glm::vec3 & newPos)
{
	addPosition(-middlePoint);

	for (int i = 0; i < 8; i++)	direction[i] += newPos; //actualizes position of points
	for (int i = 0; i < 12; i++) {
		sides[i].point.position+=newPos; //actualizes position of sides
		sides[i].pointFinal.position += newPos;
	}
	for (int i = 0; i < 6; i++) 
		square[i].Face.setPosition(glm::vec3(direction[(int)(square[i].VertexIndex.x)].x, direction[(int)(square[i].VertexIndex.x)].y, direction[(int)(square[i].VertexIndex.x)].z)); //recalculates dconst of plane
}

void CubeClass::addPosition(const glm::vec3 & newPos)
{
	for (int i = 0; i < 8; i++)	direction[i] += newPos; //actualizes position of points
	for (int i = 0; i < 12; i++) {
		sides[i].point.position += newPos; //actualizes position of sides
		sides[i].pointFinal.position += newPos;
	}
	for (int i = 0; i < 6; i++)
		square[i].Face.setPosition(glm::vec3(direction[(int)(square[i].VertexIndex.x)].x, direction[(int)(square[i].VertexIndex.x)].y, direction[(int)(square[i].VertexIndex.x)].z)); //recalculates dconst of plane
	calcMiddlePoint();
}

bool CubeClass::isInside(const glm::vec3 & point)
{
	PointClass ProjectedPoint;
	PointClass pointInLine;
	//nos guardamos el punto proyectado en la cara frontal(i=0) y cara lateral derecha(i=1)
	for (int i = 0; i < 2; i++) {
		
		//Hacemos proyeccion en el plano
		ProjectedPoint.position = point;
		square[i].Face.closestPointInPlane(ProjectedPoint.position);

		//Hacemos que las 2 rectas coincidan con los lados que definen la cara (lineas perpendiculares)
		LineClass line1(direction[(int)square[i].VertexIndex.x], direction[(int)square[i].VertexIndex.y] - direction[(int)square[i].VertexIndex.x]);
		LineClass line2(direction[(int)square[i].VertexIndex.x], direction[(int)square[i].VertexIndex.w] - direction[(int)square[i].VertexIndex.x]);

		//Combinacion lineal para ver si estan dentro de la cara
		pointInLine.position = line1.closestPointInLine(ProjectedPoint.position);
		float alpha = line1.alphaPoint(pointInLine.position);
		if (alpha > 1 || alpha < 0) return false; //esta fuera de la primera cara asi que nada

		pointInLine.position = line2.closestPointInLine(ProjectedPoint.position);
		alpha = line2.alphaPoint(pointInLine.position);
		if (alpha > 1 || alpha < 0) return false; //esta fuera de la primera cara asi que nada
	}
	return true;
}

bool CubeClass::CollisionPlane(PlaneClass plane)
{
	for (int i = 0; i < 8; i++)
		if (abs(plane.distPoint2Plane(direction[i])<=Tollerance)) return true;

	return false;
}

bool CubeClass::CollisionLine(LineClass line, PointClass &Ptall)
{
	PointClass returnValue;
	PointClass ProjectedPoint;
	PointClass pointInLine;

	for (int i = 0; i < 2; i++) {


		if (!square[i].Face.intersecLinePlane(line, ProjectedPoint.position)) {
			continue; // si el punto no esta en el segmento, no lo consideramos
		}

		//Hacemos que las 2 rectas coincidan con los lados que definen la cara (lineas perpendiculares)
		LineClass line1(direction[(int)square[i].VertexIndex.x], direction[(int)square[i].VertexIndex.y] - direction[(int)square[i].VertexIndex.x]);
		LineClass line2(direction[(int)square[i].VertexIndex.x], direction[(int)square[i].VertexIndex.w] - direction[(int)square[i].VertexIndex.x]);

		//Combinacion lineal para ver si estan dentro de la cara
		pointInLine.position = line1.closestPointInLine(ProjectedPoint.position);
		float alpha1 = line1.alphaPoint(pointInLine.position);


		pointInLine.position = line2.closestPointInLine(ProjectedPoint.position);
		float alpha2 = line2.alphaPoint(pointInLine.position);

		if (alpha1>0 && alpha2 >0 && alpha1<1 && alpha2<1) return true;
	}

	return false;
}

bool CubeClass::CollisionSegment(LineClass line, PointClass & Ptall)
{
	PointClass returnValue;
	PointClass ProjectedPoint;
	PointClass pointInLine;

	for (int i = 0; i < 6; i++) {


		if (!square[i].Face.intersecSegment(line.point.position, line.point.position+line.direction, ProjectedPoint.position)) {
			continue; // si el punto no esta en el segmento, no lo consideramos
		}

		//si intersecan, comprobamos que el punto de intersección pertenezca al segmento.
		float alpha = line.alphaPoint(ProjectedPoint.position);
		if (alpha > 1 || alpha < 0) {
			continue;
		}

		//Hacemos que las 2 rectas coincidan con los lados que definen la cara (lineas perpendiculares)
		LineClass line1(direction[(int)square[i].VertexIndex.x], direction[(int)square[i].VertexIndex.y] - direction[(int)square[i].VertexIndex.x]);
		LineClass line2(direction[(int)square[i].VertexIndex.x], direction[(int)square[i].VertexIndex.w] - direction[(int)square[i].VertexIndex.x]);

		//Combinacion lineal para ver si estan dentro de la cara
		pointInLine.position = line1.closestPointInLine(ProjectedPoint.position);
		float alpha1 = line1.alphaPoint(pointInLine.position);


		pointInLine.position = line2.closestPointInLine(ProjectedPoint.position);
		float alpha2 = line2.alphaPoint(pointInLine.position);
		
		if(alpha1>0 && alpha2 >0 && alpha1<1 && alpha2<1) return true;
	}

	return false;
}

glm::vec3 CubeClass::getVertex(int index)
{
	return direction[index] ;
}
