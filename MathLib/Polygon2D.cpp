#include "Polygon2D.h"

#include <vector>
#include "glm\gtc\constants.hpp"

glm::vec3* calcCircleVertex(int numvertex, float rad, glm::vec3 center) {
	std::vector<glm::vec3> temp;

	float angle = 360.0f / (float)numvertex;
	float angleinrad = glm::radians(angle);

	float deltaangle = 0;
	for (int i = 0; i < numvertex; i++) {
		temp.push_back(glm::vec3(center.x + rad*cos(deltaangle), center.y + rad*sin(deltaangle), 0.0f));
		deltaangle += angleinrad;
	}
	glm::vec3* retval = new glm::vec3[temp.size()];
	for (int i = 0; i < temp.size(); i++)retval[i] = temp[i];

	return retval;
}


glm::vec3* calcPolyVertex(int numvertex, float height, float width, glm::vec3 center) {
	std::vector<glm::vec3> temp;

	float angle = 360.0f / (float)numvertex;
	float angleinrad = glm::radians(angle);

	float deltaangle = glm::radians((numvertex==4)?45.0f:90.0f);
	for (int i = 0; i < numvertex; i++) {
		temp.push_back(glm::vec3(center.x + (width/2)*cos(deltaangle), center.y + (height/2)*sin(deltaangle), 0.0f));
		deltaangle += angleinrad;
	}

	glm::vec3* retval = new glm::vec3[temp.size()];
	for (int i = 0; i < temp.size(); i++)retval[i] = temp[i];

	return retval;
}


void Polygon2D::calcAveragePoint()
{
	glm::vec3 temp(0.0f, 0.0f, 0.0f);
	for (int i = 0; i < numvertex; i++) temp += vertex[i];
	temp /= numvertex;
	AveragePoint = temp;
}

void Polygon2D::calcdistCenterTofarestVertex()
{
	float dist= std::numeric_limits<float>::min();
	for (int i = 0; i < numvertex; i++) {
		float norm = glm::length(vertex[i] - AveragePoint);
		if (dist < norm) dist = norm;
	}
	distCenterTofarestVertex = dist+0.001f; //tollerance
}

Polygon2D::Polygon2D(int numvertex, glm::vec3 vert[]) : numvertex(numvertex)
{
	vertex = new glm::vec3[numvertex];
	sides = new Line2D[numvertex];
	for (int i = 0; i < this->numvertex; i++)
		vertex[i] = vert[i];

	for (int i = 0; i < this->numvertex - 1; i++) {

		LineClass temp(vertex[i], vertex[i + 1] - vertex[i]);
		sides[i].line = temp;
		glm::vec3 tempN(sides[i].line.direction.y, -sides[i].line.direction.x, 0.0f);
		sides[i].normal = tempN / glm::length(tempN);
		sides[i].d = glm::dot(-sides[i].normal, vertex[i]);
	}
	LineClass temp(vertex[numvertex - 1], vertex[0] - vertex[numvertex - 1]);
	sides[numvertex - 1].line = temp;
	glm::vec3 tempN(sides[numvertex - 1].line.direction.y, -sides[numvertex - 1].line.direction.x, 0.0f);
	sides[numvertex - 1].normal = tempN / glm::length(tempN);
	sides[numvertex - 1].d = glm::dot(-sides[numvertex - 1].normal, vertex[numvertex - 1]);

	triangulationPolygon = new glm::vec3[(numvertex - 2) * 3];
	for (int i = 0; i < (numvertex - 2) * 3; i += 3) {
		triangulationPolygon[i] = vertex[0];
		triangulationPolygon[i + 1] = vertex[i / 3 + 2];
		triangulationPolygon[i + 2] = vertex[i / 3 + 1];
	}

	calcAveragePoint();
	calcdistCenterTofarestVertex();
}

Polygon2D::~Polygon2D()
{
	/*if (vertex != NULL) delete[] vertex;
	if (sides != NULL) delete[] sides;
	if (triangulationPolygon != NULL) delete[] triangulationPolygon;*/
}

Polygon2D::Polygon2D(const Polygon2D & p)
{
	numvertex = p.numvertex;
	vertex = p.vertex;
	sides = p.sides;
	triangulationPolygon = p.triangulationPolygon;
	AveragePoint = p.AveragePoint;
	distCenterTofarestVertex = p.distCenterTofarestVertex;

}

int Polygon2D::getNumVertex()
{
	return (numvertex - 2) * 3;
}

glm::vec3 * Polygon2D::getVertex()
{
	return vertex;
}

glm::vec3 * Polygon2D::getPolygon()
{
	return  triangulationPolygon;
}

Line2D Polygon2D::getLine2D(int i) {
	return sides[i];
}

void Polygon2D::translatePoly(glm::vec3 translation)
{
	for (int i = 0; i < (numvertex - 2) * 3; i++) {
		triangulationPolygon[i] += translation;

		if (i < numvertex){
			vertex[i] += translation;
			sides[i].line.point.position += translation;
		}
	}
	AveragePoint += translation;
}
