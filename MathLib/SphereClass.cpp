#include "SphereClass.h"
#include <iostream>


SphereClass::SphereClass()
{
}

SphereClass::SphereClass(const glm::vec3 & point, const float & radious)
{
	center = point;
	radi = radious;
}

SphereClass::~SphereClass()
{
}

void SphereClass::setPosition(const glm::vec3 & newPos)
{
	center = newPos;
}

bool SphereClass::isInside(const glm::vec3 & point)
{
	float dist = glm::length(point - center);
	if (dist > radi) return false;
	return true;
}

bool SphereClass::intersecSegment(const glm::vec3 & point1, const glm::vec3 & point2, glm::vec3 & pTall)
{
	PointClass p(point1.x, point1.y, point1.z), q(point2.x, point2.y, point2.z);
	LineClass segment(p, q);
	
	
	//pTall = segment.closestPointInLine(center);
	//pTall = ;
	
	if (abs(segment.distLine2Point(center) - Tollerance) < radi) {
		if (   (abs(p.distPoint2Point(center) - Tollerance) < radi) != abs((q.distPoint2Point(center) - Tollerance) < radi)   ) {
			const float C = pow(radi, 2) - pow(center.x, 2) - pow(center.y, 2) - pow(center.z, 2);
			const float F = C - pow(p.position.x, 2) + 2 * center.x*p.position.x - pow(p.position.y, 2) + 2 * center.y*p.position.y - pow(p.position.z, 2) + 2 * p.position.z*p.position.z;

			float A = pow(segment.direction.x, 2) + pow(segment.direction.y, 2) + pow(segment.direction.z, 2);
			float B = 2 * (segment.direction.x*(p.position.x - center.x) + segment.direction.y*(p.position.y - center.y) + segment.direction.z*(p.position.z - center.z));

			float alfa1 = ((-B / 2) + sqrt((pow(B, 2) / 4) - A*F)) / A;
			float alfa2 = ((-B / 2) - sqrt((pow(B, 2) / 4) - A*F)) / A;
			
			if (alfa1 < alfa2) alfa1 = alfa2;

			float x = p.position.x + alfa1*segment.direction.x;
			float y = p.position.y + alfa1*segment.direction.y;
			float z = p.position.z + alfa1*segment.direction.z;
			
			pTall = glm::vec3(x,y,z);
			//std::cout << "\nAlfa1: " << alfa1 << " alfa2: " << alfa2 << "El Punto es: "<<x<<", "<<y<<", "<<z<<std::endl;
		}
		return (abs(p.distPoint2Point(center) - Tollerance) < radi) != abs((q.distPoint2Point(center) - Tollerance) < radi);
	}
	return false;
}

bool SphereClass::intersecSegment(const glm::vec3 & point1)
{
	//std::cout << OperationsClass::DistPoint2Point(point1, center) - pow(radi, 2) << " ";
	return abs(PointClass::DistPoint2Point(point1, center) - pow(radi, 2)) < Tollerance;
//	return ( OperationsClass::DistPoint2Point(point1, center) >=pow(radi,2)-Tollerance && OperationsClass::DistPoint2Point(point1, center) <= pow(radi, 2) + Tollerance);
}

