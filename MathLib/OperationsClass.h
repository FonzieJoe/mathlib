#pragma once
#include "GeometryClass.h"
#include "PointClass.h"
#include "LineClass.h"
#include "PlaneClass.h"
#include "TriangleClass.h"
#include "SphereClass.h"

#define _USE_MATH_DEFINES
#include <math.h>


class OperationsClass
{
public:
	OperationsClass();
	~OperationsClass();

	static bool InterseccionRectaTriangulo(glm::vec3 v1, glm::vec3 v2, glm::vec3 v3, glm::vec3 Punto);
	
};

