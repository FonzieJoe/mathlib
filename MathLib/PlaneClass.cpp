#include "PlaneClass.h"



PlaneClass::PlaneClass()
{
}


PlaneClass::~PlaneClass()
{
}

PlaneClass::PlaneClass(const glm::vec3 & point, const glm::vec3 & normalVect)
{
	normal = glm::normalize(normalVect);
	dconst = -glm::dot(point, normal);
}

PlaneClass::PlaneClass(const glm::vec3 & point0, const glm::vec3 & point1, const glm::vec3 & point2)
{
	glm::vec3 v1 = point1 - point0;
	glm::vec3 v2 = point2 - point0;
	normal = glm::normalize(glm::cross(v1, v2));
	dconst = -glm::dot(point0, normal);
}

void PlaneClass::setPosition(const glm::vec3 & newPos)
{
	dconst = -glm::dot(newPos, normal);
}

bool PlaneClass::isInside(const glm::vec3 & point)
{
	return normal.x*point.x+normal.y*point.y+normal.z*point.z+dconst<Tollerance;
}

float PlaneClass::distPoint2Plane(const glm::vec3 & point)
{
	return normal.x*point.x + normal.y*point.y + normal.z*point.z + dconst / glm::length(normal);
}

glm::vec3 PlaneClass::closestPointInPlane(const glm::vec3 & point)
{
	LineClass Pline(point, normal);
	glm::vec3 pTall;
	intersecLinePlane(Pline, pTall);

	return pTall;
}

bool PlaneClass::intersecSegment(const glm::vec3 & punt1, const glm::vec3 & punt2, glm::vec3 & pTall)
{
	PointClass p(punt1.x, punt1.y, punt1.z);
	PointClass q(punt2.x, punt2.y, punt2.z);
	LineClass line(p, q);
	if (abs(glm::dot(normal, line.direction)) < Tollerance) //producto escalar=0 --> 90 grados
		return 0;

	float Alpha = -(dconst + normal.x*line.point.position.x + normal.y*line.point.position.y + normal.z*line.point.position.z) / (normal.x*line.direction.x + normal.y*line.direction.y + normal.z*line.direction.z);
	pTall = glm::vec3(line.direction.x*Alpha + line.point.position.x, line.direction.y*Alpha + line.point.position.y, line.direction.z*Alpha + line.point.position.z);
	return (Alpha >= 0 - Tollerance && Alpha <= 1 + Tollerance);
}

bool PlaneClass::intersecLinePlane(const LineClass & line, glm::vec3 & pTall)
{

	if (abs(glm::dot(normal, line.direction)) < Tollerance) //producto escalar=0 --> 90 grados
		return 0;

	float Alpha;

	Alpha = -(dconst + normal.x*line.point.position.x + normal.y*line.point.position.y + normal.z*line.point.position.z) / (normal.x*line.direction.x + normal.y*line.direction.y + normal.z*line.direction.z);

	pTall= glm::vec3(line.direction.x*Alpha + line.point.position.x, line.direction.y*Alpha + line.point.position.y, line.direction.z*Alpha + line.point.position.z);

	return 1;
}

glm::vec3 PlaneClass::intersecLinePlane(PointClass Origen, glm::vec3 VDirectorRecta)
{
	float Alpha;
	
	Alpha = -dconst / (VDirectorRecta.x*normal.x + VDirectorRecta.y*normal.y + VDirectorRecta.z*normal.z);

	return glm::vec3(VDirectorRecta.x*Alpha + Origen.position.x, VDirectorRecta.y*Alpha + Origen.position.y, VDirectorRecta.z*Alpha + Origen.position.z);
}

bool PlaneClass::intersecLineLine(const LineClass line, glm::vec3 & pTall)
{
	return false;
}

float PlaneClass::distLine2Plane(LineClass line)
{
	if (abs(glm::dot(line.direction, normal)) < Tollerance) {
		
		float temp1 = normal.x*line.point.position.x + normal.y*line.point.position.y + normal.z*line.point.position.z + dconst;
		float temp2 = sqrt(pow(normal.x,2)+ pow(normal.y, 2)+ pow(normal.z, 2));
		
		if (temp2 != 0) {
			return temp1 / temp2;
		}
		else {
			return 0.0f;
		}
	}
	else {
		return 0.0f;
	}

}
