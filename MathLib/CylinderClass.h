#pragma once
#include "GeometryClass.h"
#include "PlaneClass.h"
#include "SphereClass.h"
class CylinderClass :
	public GeometryClass
{
	PointClass p1, p2;
	LineClass line;
	float rad;
public:
	CylinderClass();
	CylinderClass(PointClass _p1, PointClass _p2, float _rad);
	~CylinderClass();

	///intersection
	bool isInside(const glm::vec3& point);
	bool intersectionLine(LineClass line, glm::vec3 ptall[]);
	
	//extra
	bool intersectionPlane(glm::vec3 ptall[], PlaneClass plane);
	bool intersectionSphere(SphereClass Sphere);
	bool intersectionCylinder(CylinderClass cyl);
	bool intersectionTriangle(TriangleClass tri);

	///distance
	float dist2point(PointClass p);
	float dist2line(LineClass l);
	float dist2Plane(PlaneClass p);
	float dist2Cylinder(CylinderClass c);
	
	//extra
	glm::vec3 dist2sphere(SphereClass s);
	glm::vec3 dist2cylinder(CylinderClass c);
	glm::vec3 dist2Triangle(TriangleClass t);


	void setPosition(const glm::vec3& newPos);
};

