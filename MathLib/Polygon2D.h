#pragma once

#include <GL/glew.h>			
#include <glm/glm.hpp>
#include "GeometryClass.h"
#include "LineClass.h"


#include <iostream>

glm::vec3* calcCircleVertex(int numvertex, float rad, glm::vec3 center);
glm::vec3* calcPolyVertex(int numvertex, float height, float width, glm::vec3 center);

struct Line2D{ //used for collisions
	LineClass line;
	glm::vec3 normal;
	float d;
};

class Polygon2D
{
	int numvertex;
	glm::vec3 * vertex;
	Line2D * sides;
	glm::vec3* triangulationPolygon;

	glm::vec3 AveragePoint;
	float distCenterTofarestVertex;

	void calcAveragePoint();
	void calcdistCenterTofarestVertex();

public:
	Polygon2D(int numvertex, glm::vec3 vert[]);
	~Polygon2D();
	Polygon2D(const Polygon2D&p);

	int getNumVertex();  //returns number of vertices considering that the polygon is built with triangles
	glm::vec3* getVertex(); 
	glm::vec3* getPolygon();
	Line2D getLine2D(int i);
	glm::vec3 getAveragePoint() { return AveragePoint; }
	float getDistCenterToVertex() { return distCenterTofarestVertex; }
	Line2D* getSides() { return sides; }
	int getNumSides() { return numvertex; }
	void translatePoly(glm::vec3 translation);

};

