#include "OperationsClass.h"



OperationsClass::OperationsClass()
{
}


OperationsClass::~OperationsClass()
{
}






bool OperationsClass::InterseccionRectaTriangulo(glm::vec3 v1, glm::vec3 v2, glm::vec3 v3, glm::vec3 Punto)
{
	float D1, D2, D3;
	D1 = ((Punto.x - v1.x)*(v3.y - v1.y) - (Punto.y - v1.y)*(v3.x - v1.x));
	D2 = ( (v1.x-Punto.x)*(v2.y-v1.y)-(v1.y-Punto.y)*(v2.x-v1.x) );
	D3 = ( (v2.x-v1.x)*(v3.y-v1.y)-(v2.y-v1.y)*(v3.x-v1.x) );

	float a = D1 / D3;
	float b = D2 / D3;

	return (a >= 0 && b >= 0 && a + b <= 1);
}
