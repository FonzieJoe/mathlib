#include "PointClass.h"
#define _USE_MATH_DEFINES
#include <math.h>


PointClass::PointClass()
{
}

PointClass::PointClass(const float & x, const float & y, const float & z)
{
	position.x = x;
	position.y = y;
	position.z = z;
}

PointClass::PointClass(const float angleA, const float angleB)
{
	float Alpha = (angleA*M_PI) / 180.0f;
	float Beta = (angleB*M_PI) / 180.0f;
	glm::vec3 pos((cos(Alpha) * cos(Beta)), (sin(Beta)), (sin(Alpha) * sin(Beta)));
	position = pos;
}

PointClass::PointClass(const glm::vec3 & newPos)
{
	position = newPos;
}


PointClass::~PointClass()
{
}

void PointClass::setPosition(const glm::vec3 & newPos)
{
	position = newPos;
}

void PointClass::setPosition(const float & x, const float & y, const float & z)
{
	position.x = x;
	position.y = y;
	position.z = z;
}



glm::vec3 PointClass::Direction(glm::vec3 Point1, glm::vec3 Point2)
{
	return glm::vec3(Point2 - Point1);
}

float PointClass::DistPoint2Point(glm::vec3 Point1, glm::vec3 Point2)
{
	return (pow((Point1.x - Point2.x), 2) + pow((Point1.y - Point2.y), 2) + pow((Point1.z - Point2.z), 2));
}

bool PointClass::isInside(const glm::vec3 & point)
{
	return false;
}

float PointClass::distPoint2Point(const PointClass & punt)
{
	float dist;
	return dist = glm::length(this->position - punt.position);
}

PointClass PointClass::pointInSegment(const PointClass & q, const float & alfa)
{
	PointClass r = (1 - alfa)*(this->position) + alfa*q.position;
	return r;
}

void PointClass::SetPolarCoords(float Alpha, float Beta)
{
	Alpha = (Alpha*M_PI) / 180.0f;
	Beta = (Beta*M_PI) / 180.0f;
	glm::vec3 pos((cos(Alpha) * cos(Beta)), (sin(Beta)), (sin(Alpha) * sin(Beta)));
	position = pos;
}
