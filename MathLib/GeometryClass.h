#pragma once
#include <glm\glm.hpp>
#include <math.h>

#define Tollerance pow(10,-7)

class GeometryClass
{
	

public:
	GeometryClass();
	~GeometryClass();
	virtual void setPosition(const glm::vec3& newPos) = 0;
	virtual bool isInside(const glm::vec3& point) = 0;
};

