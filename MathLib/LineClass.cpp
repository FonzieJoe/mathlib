#include "LineClass.h"
#define _USE_MATH_DEFINES
#include <math.h>

LineClass::LineClass()
{
}

LineClass::~LineClass()
{
}

LineClass::LineClass(const glm::vec3 & origin, const glm::vec3 & vector) //VERIFIED
{
	point.position = origin;
	direction = vector;
	pointFinal.position = origin + direction;
}

LineClass::LineClass(const PointClass & origen, const PointClass & extrem) //VERIFIED
{
	point.position = origen.position;
	direction = extrem.position - origen.position;
	pointFinal.position = extrem.position;
}

LineClass::LineClass(const glm::vec3 & origin, float angle1, float angle2)
{
	point.position = origin;
	setPolarDirection(angle1, angle2);
}


void LineClass::setPosition(const glm::vec3 & newPos) //VERIFIED
{
	point.position = newPos;
}

void LineClass::setDirection(const glm::vec3 & newDir) //VERIFIED
{
	direction = newDir;
}

void LineClass::setPolarDirection(float angle1, float angle2)
{
	angle1 = (angle1*M_PI) / 180.0f;
	angle2 = (angle2*M_PI) / 180.0f;
	glm::vec3 pos((cos(angle1) * cos(angle2)), (sin(angle2)), (sin(angle1) * sin(angle2)));
	pointFinal.position = pos;
	direction = (pointFinal.position - point.position);
}

bool LineClass::isInside(const PointClass & punt) //punto interno recta //VERIFIED
{
	return  distLine2Point(punt) < Tollerance;
}

bool LineClass::isInside(const glm::vec3 & punt) // coordenadas pertenecientes a recta //VERIFIED
{
	PointClass p(punt.x, punt.y, punt.z);
	return distLine2Point(p) < Tollerance;
}

float LineClass::distLine2Point(const PointClass & punt) //VERIFIED
{
	return glm::length(glm::cross(punt.position - point.position, direction)) / glm::length(direction);
}

float LineClass::distSegment2Point(PointClass & punt)
{
	glm::vec3 closestPoint = closestPointInLine(punt);
	float alpha = alphaPoint(closestPoint);
	if (alpha >= 1) return punt.distPoint2Point(pointFinal);
	if (alpha <= 0) return punt.distPoint2Point(point);
	
	return distLine2Point(punt);
}

float LineClass::projectPointToLine(const PointClass & point) //((R - P)*v) / (v*v) = Alpha(punto)--->punto = P + alpha(punto)*v-------- - R->punto espacio, P->puntop recta //VERIFIED
{//calculo el alpha
	return glm::dot(point.position-this->point.position,direction)/glm::dot(direction,direction);
}

glm::vec3 LineClass::closestPointInLine(const PointClass & punt) //VERIFIED
{
	float alpha = projectPointToLine(punt);
	return glm::vec3(point.position+alpha*direction);
}

float LineClass::distLine2Line(const LineClass & line) //VERIFIED
{
	float magnitude = glm::length(glm::cross(direction, line.direction));

	if (magnitude < Tollerance) //paralelas
		return glm::length(line.point.position - point.position);

	//no paralelas
	glm::mat3 matrix(line.point.position - point.position, direction, line.direction);
	float det = abs(glm::determinant(matrix));
	return det/magnitude;
}

bool LineClass::Line2LineIntersection(LineClass & line, glm::vec3 & inter) //VERIFIED
{
	//http://www.youmath.it/domande-a-risposte/view/6185-rette-sghembe-nello-spazio.html
	glm::vec3 first(line.point.position.x - point.position.x, line.point.position.y - point.position.y, line.point.position.z - point.position.z);
	glm::vec3 second = direction;
	glm::vec3 third = line.direction;

	glm::mat3 matrix(first, second, third);
	float detMatrix = glm::determinant(matrix); // if equal to 0, lines are in different planes

	if (glm::length(glm::cross(line.direction, direction))!=0 && abs(detMatrix)<Tollerance) 
	{
		//http://mathforum.org/library/drmath/view/62814.html

		glm::vec3 temp = line.point.position - point.position;

		glm::vec3 drMath = glm::cross(temp, line.direction);
		glm::vec3 drMath2 = glm::cross(direction, line.direction);

		float angle1 = 0, angle2 = 0;
		//check if vectors of cross product have same verse
		const float PI = 3.1415927;
		float angle = 1 / cos(glm::length(drMath)*glm::length(drMath2) / glm::dot(drMath, drMath2)) * 180 / PI; 

		float alpha = glm::length(drMath) / glm::length(drMath2);
		alpha *= -pow(-1, (angle >= 0 - Tollerance && angle <= 180 + Tollerance));

		inter = point.position + alpha*direction;
		return true;
	}
	return false;
}

float LineClass::alphaPoint(const glm::vec3 & p) //VERIFIED
{
	glm::vec3 temp=((p - point.position));
	float alpha= glm::length(temp) / glm::length(direction);
	alpha *= (temp.x*direction.x >= 0 && temp.y*direction.y >= 0 && temp.z*direction.z >= 0) ? 1 : -1;


	return alpha;//glm::length(temp)/glm::length(direction);
}

bool LineClass::segment2SegmentIntersection( LineClass & line, glm::vec3 &  inter) //VERIFIED
{
	if (Line2LineIntersection(line, inter)) {
		float alpha1 = alphaPoint(inter);
		float alpha2 = line.alphaPoint(inter);

		return (alpha1 >= 0 - Tollerance && alpha1 <= 1 + Tollerance && alpha2 >= 0 - Tollerance && alpha2 <= 1 + Tollerance);
	}
	return false;

}
