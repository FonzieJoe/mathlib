#include "TriangleClass.h"



TriangleClass::TriangleClass()
{
}

TriangleClass::TriangleClass(const glm::vec3 & point0, const glm::vec3 & point1, const glm::vec3 & point2)
{
	vertex1 = point0;
	vertex2 = point1;
	vertex3 = point2;
	normal = glm::normalize(glm::cross(vertex2 - vertex1, vertex3 - vertex2));
}

void TriangleClass::setPosition(const glm::vec3 & newPos)
{
	glm::vec3 baryc;
	baryc = (vertex1 + vertex2 + vertex3) / 3.0f;     //displacement of the barycenter 
	glm::vec3 disp = newPos - baryc; //is only considered
	vertex1 = vertex1 + disp;
	vertex2 = vertex2 + disp;
	vertex3 = vertex3 + disp;
}

bool TriangleClass::isInside(const glm::vec3 & point)
{
	float D1, D2, D3;
	D1 = ((point.x - vertex1.x)*(vertex3.y - vertex1.y) - (point.y - vertex1.y)*(vertex3.x - vertex1.x));
	D2 = ((vertex1.x - point.x)*(vertex2.y - vertex1.y) - (vertex1.y - point.y)*(vertex2.x - vertex1.x));
	D3 = ((vertex2.x - vertex1.x)*(vertex3.y - vertex1.y) - (vertex2.y - vertex1.y)*(vertex3.x - vertex1.x));

	float a = D1 / D3;
	float b = D2 / D3;

	return (a >= 0 && b >= 0 && a + b <= 1);
}

bool TriangleClass::intersecSegment(const glm::vec3 & point1, const glm::vec3 & point2, glm::vec3 & pTall)
{
	PlaneClass planeTriang(vertex1, vertex2, vertex3);

	if (planeTriang.intersecSegment(point1, point2, pTall)) {
		return isInside(pTall);
	}
	return false;
}

glm::vec3 TriangleClass::PlanoPertenecienteTriangulo()
{
	return glm::vec3(glm::cross(vertex2 - vertex1, vertex3 - vertex1));
}

bool TriangleClass::InterseccionRectaTriangulo(glm::vec3 Punto)
{
	float D1, D2, D3;
	D1 = ((Punto.x - vertex1.x)*(vertex3.y - vertex1.y) - (Punto.y - vertex1.y)*(vertex3.x - vertex1.x));
	D2 = ((vertex1.x - Punto.x)*(vertex2.y - vertex1.y) - (vertex1.y - Punto.y)*(vertex2.x - vertex1.x));
	D3 = ((vertex2.x - vertex1.x)*(vertex3.y - vertex1.y) - (vertex2.y - vertex1.y)*(vertex3.x - vertex1.x));

	float a = D1 / D3;
	float b = D2 / D3;

	return (a >= 0 && b >= 0 && a + b <= 1);
}


TriangleClass::~TriangleClass()
{
}
