#pragma once
#include "GeometryClass.h"

class PointClass : public GeometryClass {
	


public:
	glm::vec3 position;

	PointClass();
	PointClass(const float& x, const float& y, const float& z);
	PointClass(const float angleA, const float angleB); //coordenadas esfericas
	PointClass(const glm::vec3& newPos);
	~PointClass();

	void setPosition(const glm::vec3& newPos);
	void setPosition(const float& x, const float& y, const float& z);
	static glm::vec3 Direction(glm::vec3 Point1, glm::vec3 Point2);
	static float DistPoint2Point(glm::vec3 Point1, glm::vec3 Point2);
	bool isInside(const glm::vec3& point);
	float distPoint2Point(const PointClass& punt);
	PointClass pointInSegment(const PointClass& q, const float& alfa);

	void SetPolarCoords(float Alpha, float Beta);
};

