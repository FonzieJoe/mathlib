#pragma once
#include "GeometryClass.h"
#include "PointClass.h"

class LineClass :  //VERIFIED. ALL METHODS WORKING AS THEY SHOULD
	public GeometryClass
{
	

public:
	PointClass point;
	glm::vec3 direction;
	PointClass pointFinal;

	LineClass();
	~LineClass();

	
	LineClass(const glm::vec3& origin, const glm::vec3& vector);
	LineClass(const PointClass& origen, const PointClass& extrem);
	LineClass(const glm::vec3& origin, float angle1, float angle2);

	void setPosition(const glm::vec3& newPos);
	void setDirection(const glm::vec3& newDir);
	void setPolarDirection(float angle1, float angle2);
	bool isInside(const PointClass& punt);
	bool isInside(const glm::vec3& punt);
	float distLine2Point(const PointClass& punt);
	float distSegment2Point(PointClass& punt);
	float projectPointToLine(const PointClass& point);				//punto proyectado en linea  ((R-P)*v)/(v*v)=Alpha(punto) ---> punto=P+alpha(punto)*v --------- R->punto espacio, P -> puntop recta
	glm::vec3 closestPointInLine(const PointClass& punt);			//llamamos al metodo de arriba
	float distLine2Line(const LineClass& line);
	bool Line2LineIntersection(LineClass& line, glm::vec3 & inter);

	float alphaPoint(const glm::vec3& p);

	bool segment2SegmentIntersection( LineClass& line, glm::vec3 & inter);
};

