#pragma once
#include "GeometryClass.h"
#include "LineClass.h"

class PlaneClass :
	public GeometryClass
{
public:
	glm::vec3 normal;
	float dconst;

	PlaneClass();
	~PlaneClass();
	
	PlaneClass(const glm::vec3& point, const glm::vec3& normalVect);
	PlaneClass(const glm::vec3& point0, const glm::vec3& point1, const glm::vec3& point2);

	void setPosition(const glm::vec3& newPos);
	bool isInside(const glm::vec3& point);
	float distPoint2Plane(const glm::vec3& point);
	glm::vec3 closestPointInPlane(const glm::vec3& point);
	bool intersecSegment(const glm::vec3& punt1, const glm::vec3& punt2, glm::vec3& pTall);
	bool intersecLinePlane(const LineClass& line, glm::vec3& pTall);
	glm::vec3 intersecLinePlane(PointClass Origen, glm::vec3 VDirectorRecta);
	bool intersecLineLine(const LineClass line, glm::vec3& pTall);
	float distLine2Plane(LineClass line);
};

