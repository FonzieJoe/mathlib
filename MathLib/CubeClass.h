#pragma once
#include "GeometryClass.h"
#include "PlaneClass.h"
#include <iostream>

struct Square {
	PlaneClass Face;
	glm::vec4 VertexIndex;
	Square() { }
	Square(int a, int b, int c, int d) {
			VertexIndex = glm::vec4(a, b, c, d);
	}
};

class CubeClass :
	public GeometryClass
{
	void calcMiddlePoint();

public:
	CubeClass();
	CubeClass(glm::vec3 dir[8]);
	~CubeClass();


	glm::vec3 middlePoint;
	glm::vec3 direction[8];
	LineClass sides[12];
	Square square[6];

	//position
	void setPosition(const glm::vec3& newPos);
	void addPosition(const glm::vec3& newPos);


	//collisions
	bool isInside(const glm::vec3& point);
	bool CollisionPlane(PlaneClass plane);
	bool CollisionLine(LineClass line, PointClass &Ptall);
	bool CollisionSegment(LineClass line, PointClass &Ptall);
	
	//getters
	glm::vec3 getVertex(int index);
};

