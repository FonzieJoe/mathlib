#pragma once
#include "GeometryClass.h"
#include "OperationsClass.h"

class SphereClass :
	public GeometryClass
{
public:

	glm::vec3 center;
	float radi;
	SphereClass();
	SphereClass(const glm::vec3& point, const float& radious);
	~SphereClass();
	void setPosition(const glm::vec3& newPos);
	bool isInside(const glm::vec3& point);
	bool intersecSegment(const glm::vec3& point1, const glm::vec3& point2, glm::vec3& pTall);
	bool intersecSegment(const glm::vec3& point1);
};

